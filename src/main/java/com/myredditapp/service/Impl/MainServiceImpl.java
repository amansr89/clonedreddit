package com.myredditapp.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.myredditapp.dto.TopicUserDTO;
import com.myredditapp.model.Topic;
import com.myredditapp.model.User;
import com.myredditapp.service.MainService;
import com.myredditapp.util.DataObject;

@Service
public class MainServiceImpl implements MainService{

	@Override
	public List<TopicUserDTO> findTopTopicUsers() {
		// TODO Auto-generated method stub
		//finding top elements from the topicList.
		List<Topic> topTopics = null;
	    int size = DataObject.topicList.size();
	    if(size !=0 && size > 20 )
	    	topTopics = DataObject.topicList.subList(0, 20);
	    else
	    	topTopics = DataObject.topicList;
	    
		return mapTopics(topTopics);
	}

	private List<TopicUserDTO> mapTopics(List<Topic> topTopics){
		List<TopicUserDTO> topicUserDTOList = new ArrayList<TopicUserDTO>();
		int counter = 1;
	    for(Topic topic:topTopics){
	    	TopicUserDTO tuDTO = new TopicUserDTO();
	    	tuDTO.setSeq(counter++);
	    	tuDTO.setTopic(topic);
	    	tuDTO.setUserName(DataObject.topicUserMap.get(topic).getUserName());
	    	topicUserDTOList.add(tuDTO);
	    }
	    return topicUserDTOList;
	}
	@Override
	public Topic addTopic(String userName, Topic topic) {
		

		// TODO Auto-generated method stub
		if(null != topic){
			if(!StringUtils.isEmpty(userName)){
				User user = new User();
				user.setUserName(userName);
				user.setCreatedDate(new Date());
				
				/*Creating Topic and inserting this topic in 3 non persistent DataObjects
				topicList : stores the list of Topic
				topicMap : maintains a key value pair of topicId,Topic. This is done
				to quickly find the topic that has been upvoted.
				topicUserMap : maintain a map of Topic and its User
				*/
				
				if(null != topic.getMessage()){
					int id = DataObject.topicList.size() + 1;
					topic.setId(id);
					topic.setTimestamp(new Date());
					topic.setVotes(0);
					DataObject.topicList.add(topic);
					DataObject.topicMap.put(id, topic);
					DataObject.topicUserMap.put(topic, user);
				}
				
				return topic;
				
			}
		} 
		return null;
	}
	
	@Override
	public Topic voteUp(int topicId) {
		//UpVoting a Topic
		Topic topic = DataObject.topicMap.get(topicId);
		if(null != topic){
			topic.setVotes(topic.getVotes() + 1);
			return topic;
		}
		return null;
	}
	
	@Override
	public Topic voteDown(int topicId) {
		//DownVoting a Topic
		Topic topic = DataObject.topicMap.get(topicId);
		if(null != topic){
			topic.setVotes(topic.getVotes() - 1);
			return topic;
		}
		return null;
	}


}

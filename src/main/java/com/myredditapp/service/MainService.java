package com.myredditapp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myredditapp.dto.TopicUserDTO;
import com.myredditapp.model.Topic;

@Service
public interface MainService {

	public List<TopicUserDTO> findTopTopicUsers();
	public Topic addTopic(String userName, Topic topic);
	public Topic voteUp(int topicId);
	public Topic voteDown(int topicId);
	
}

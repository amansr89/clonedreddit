package com.myredditapp;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.myredditapp.dto.TopicUserDTO;
import com.myredditapp.model.Topic;
import com.myredditapp.service.MainService;
import com.myredditapp.util.DataObject;


@Controller
public class HomeController implements ErrorController{
	@Autowired
	private MainService mainService;
	
	private static final String PATH = "/error";

    @RequestMapping("/home")
    public ModelAndView home(Model model) {

    	ModelAndView mv = new ModelAndView("home");
    	
    	//We first sort on the basis of votes and then get the top 20 topics from the list
		Collections.sort(DataObject.topicList, Topic.voteComparator);

		List<TopicUserDTO> topicUserDTOList = mainService.findTopTopicUsers();

		mv.addObject("result", topicUserDTOList);
		return mv;
	}
    
    @RequestMapping("/latest")
	public ModelAndView latest() {

		ModelAndView mv = new ModelAndView("home");

		//We first sort on the basis of timestamp and then get the top 20 topics from the list
		Collections.sort(DataObject.topicList, Topic.timeComparator);

		List<TopicUserDTO> topicUserDTOList = mainService.findTopTopicUsers();

		mv.addObject("result", topicUserDTOList);
		return mv;
	}

	@RequestMapping(value = "/newTopic", method = RequestMethod.GET)
	private ModelAndView newTopic(){

		ModelAndView mv = new ModelAndView("newTopic");
		return mv;
	}

	@RequestMapping(value = "/addTopic", method = RequestMethod.POST)
	private ModelAndView addTopic(@ModelAttribute("TopicUserClass") TopicUserDTO topicUserDTO){
		//creating topic from DTO and adding to Data layer
		Topic topic = new Topic();
		topic.setMessage(topicUserDTO.getTopic().getMessage());
		topic.setTitle(topicUserDTO.getTopic().getTitle());
		topic=  mainService.addTopic(topicUserDTO.getUserName(),topic);
		return new ModelAndView("redirect:/home");
	}
	
	@RequestMapping(value = "voteup/{topicId}", method = RequestMethod.GET)
	private ModelAndView voteUp(@PathVariable("topicId") int  topicId){
		Topic topic =  mainService.voteUp(topicId);
		//In case of incorrect Access redirecting to errorPage
		if(null == topic){
			ModelAndView mv = new ModelAndView("errorPage");
			return mv;
		}
		return new ModelAndView("redirect:/home");
	}

	@RequestMapping(value = "/votedown/{topicId}", method = RequestMethod.GET)
	private ModelAndView voteDown(@PathVariable("topicId") int  topicId){
		Topic topic =  mainService.voteDown(topicId);
		//In case of incorrect Access redirecting to errorPage
		if(null == topic){
			ModelAndView mv = new ModelAndView("errorPage");
			return mv;
		}
		return new ModelAndView("redirect:/home");
	}
    @RequestMapping(value = PATH)
    public String error() {
        return "errorPage";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}

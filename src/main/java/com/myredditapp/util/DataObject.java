package com.myredditapp.util;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.myredditapp.model.Topic;
import com.myredditapp.model.User;


public  class DataObject {
	public static List<Topic> topicList = new ArrayList<Topic>();
	public static HashMap<Topic, User> topicUserMap = new HashMap<Topic, User>();
	public static HashMap<Integer, Topic> topicMap = new HashMap<Integer, Topic>();
}

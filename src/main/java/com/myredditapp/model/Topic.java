package com.myredditapp.model;

import java.util.Comparator;
import java.util.Date;

public class Topic implements Comparable<Topic>{
private int id;
private String title;
private String message;
private Integer votes;
private Date timestamp;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public Integer getVotes() {
	return votes;
}
public void setVotes(Integer votes) {
	this.votes = votes;
}

public Date getTimestamp() {
	return timestamp;
}
public void setTimestamp(Date timestamp) {
	this.timestamp = timestamp;
}
@Override
public int compareTo(Topic o) {
	// TODO Auto-generated method stub
	return o.votes - this.votes;
}

public static final Comparator<Topic> timeComparator = new Comparator<Topic>(){

    @Override
    public int compare(Topic t1, Topic t2) {
        return t2.timestamp.compareTo(t1.timestamp) ;  // This will compare on basis of time
    }
};


public static final Comparator<Topic> voteComparator = new Comparator<Topic>(){

    @Override
    public int compare(Topic t1, Topic t2) {
        return t2.votes - t1.votes ;  // This will compare on basis of votes
    }
};



}

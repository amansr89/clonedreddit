package com.myredditapp.dto;

import com.myredditapp.model.Topic;

public class TopicUserDTO {

private int seq;
private Topic topic;
private String userName;


public int getSeq() {
	return seq;
}
public void setSeq(int seq) {
	this.seq = seq;
}
public Topic getTopic() {
	return topic;
}
public void setTopic(Topic topic) {
	this.topic = topic;
}

public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}

}

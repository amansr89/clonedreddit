<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta name="referrer" content="origin">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Reddit</title>
</head>
<body>
	<center>
		<table border="0" cellpadding="0" cellspacing="0"
			width="85%" bgcolor="#ADD8E6">
			<tbody >
				<tr>
					<td bgcolor="#00ffff">
						<table border="0" cellpadding="0" cellspacing="0" width="100%"
							style="padding: 2px">
							<tbody>
								<tr>
									<td style="line-height: 12pt; height: 10px;"><span
										><b><a href="home">
										Reddit
											</a></b> | <a href="latest">Latest</a>  
											 | <a
											href="newTopic">Submit</a> </span></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="height: 10px"></tr>
				<tr>
					<td>
						<table style="table-layout: fixed;" width="85%" border="0" cellpadding="0" cellspacing="0" >
							<tbody>
								<c:forEach items="${result}" var="result">
									<tr id="${result.topic.id}">
										<td style="word-wrap:break-word;" width="2%" align="right" valign="top"><span
											>${result.seq}.</span></td>
										<td style="word-wrap:break-word;" width="2%" valign="top">
											<center>
												<a id="${result.topic.id}_up"
													href="voteup/${result.topic.id}">
													<img alt="upvote" src="up_arrow.png">
												</a>
											</center>
											<center>
												<a id="${result.topic.id}_down"
													href="votedown/${result.topic.id}">
													<img alt="downvote" src="down_arrow.png">
												</a>
											</center>
										</td>
										<td style="font-weight: bold;">${result.topic.title}</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td style="word-wrap:break-word;" style="table-layout: fixed;">${result.topic.message}</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td style="word-wrap:break-word;"><span 
											id="votes_${result.topic.id}"><b>${result.topic.votes}</b> Votes.</span>  
											<span>
											created by <b>${result.userName}</b> </span>
											<span>submitted at ${result.topic.timestamp} </span></td>
									</tr>
									<tr style="height: 5px;bgcolor:#00ffff "></tr>
									</c:forEach>
							</tbody>
						</table>
				</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>
</html>

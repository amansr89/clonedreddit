<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta name="referrer" content="origin">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Add a Topic</title>
</head>
<body>
	<center>
		<table id="hnmain" border="0" cellpadding="0" cellspacing="0"
			width="85%" bgcolor="#ADD8E6">
			<tbody>
				<tr>
					<td bgcolor="#00ffff">
						<table border="0" cellpadding="0" cellspacing="0" width="100%"
							style="padding: 2px">
							<tbody>
								<tr>
									<td style="line-height: 12pt; height: 10px;"><span
										><b><a href="home">
										Reddit
											</a></b> | <a href="latest">Latest</a>  
											 | <a
											href="newTopic">Submit</a> </span></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="height: 10px"></tr>
				<tr>
					<td>
					<form:form action="addTopic" method="post" id="form"
						commandName="TopicUserClass" onsubmit="return validateForm()">
						<div>
							<div>
								<input type="text"  name="userName" id="userName"
									placeholder="User Name" size="50" maxlength="20">
									</div>
									<div  style="margin-top: 5px">
									<div>
								<input type="text"  name="topic.title" id="topicTitle"
									placeholder="Topic Title" size="50" maxlength="20">
									</div>
									<div  style="margin-top: 5px">
									<div>
								<textarea name="topic.message" id="topicMessage"
									placeholder="Topic Message"  rows= "6" cols = "50" maxlength="255"></textarea>
								</div>
								<div  id="Error"
									style="color: red; display: none"></div>
							</div>
							<div  style="margin-top: 5px">
								<button type="submit"  value="submit">Submit</button>
							</div>
						</div>
					</form:form>
						</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>
<script type="text/javascript">
function validateForm() {
	var userName = document.getElementById("userName").value;
	var topicTitle = document.getElementById("topicTitle").value;
	var topicMessage = document.getElementById("topicMessage").value;
	if (!userName.trim() || userName.trim()==="") {
		console.log("UserName");
		document.getElementById('Error').style.cssText = 'color:red; display:block';
		document.getElementById('Error').innerHTML = "Please enter a userName.";
		return false;
	}
	if (!topicTitle.trim() || topicTitle.trim()==="") {
		document.getElementById('Error').style.cssText = 'color:red; display:block';
		document.getElementById('Error').innerHTML = "Please enter a Title.";
		return false;
	}
	if (!topicMessage.trim() || topicMessage.trim()==="") {
		document.getElementById('Error').style.cssText = 'color:red; display:block';
		document.getElementById('Error').innerHTML = "Please enter a Message.";
		return false;
	}
	return true;
}
</script>
</html>
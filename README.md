# Reddit Clone

## Heroku Link 
https://mysterious-shelf-14377.herokuapp.com

## Guide
https://amansr89@bitbucket.org/amansr89/clonedreddit.git

## What you'll need
- JDK 1.7 or later
- Maven 3 or later

## Stack
- Spring Boot
- Java



## Run Local
`mvn spring-boot:run`

## Description
- Base class : HomeController
- Reddit link on Home Page shows top 20 Topic in descending order of their votes.
- Latest link show Top 20 topics in descending order of time of submission.
- Submit link allows to add a new topic. Guest users can submit topic as there is no login.
- All the fields in Topic Submission are required.

## Assumptions
- There is no authentication/login so guest user can submit topic.
- Max length of title and userName is 20 ,  while that of Message is 255 characters
 